Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  get ':date', to: 'bets#index', constraints: { date: /\d{4}-(1[0-2]|0[1-9]|\d)-(0[1-9]|[12]\d|3[01])/ }
end
