module BetsHelper
  def pick_over_tag(pick_over)
    content_tag(:span, class: 'text-danger') do
      pick_over ? 'Over' : 'Under'
    end.html_safe
  end
end
