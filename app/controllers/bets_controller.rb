class BetsController < ApplicationController
  def index
    date = Time.zone.parse(params[:date])
    @bets = Bet.includes(:player).by_date(date)
  end
end
