class Player < ApplicationRecord
  has_many :bets, dependent: :destroy

  def full_name
    [first_name, last_name].compact.join(' ')
  end
end
