class Bet < ApplicationRecord
  belongs_to :player
  scope :by_date, -> (date) { where(start_time: date.beginning_of_day..date.end_of_day) }  
end
