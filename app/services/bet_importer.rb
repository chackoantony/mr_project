require 'csv'

class BetImporter
  def initialize(file:)
    @file = file
  end

  def import!
    CSV.foreach(@file, :headers => true) do |row|
      ActiveRecord::Base.transaction do
        player = Player.find_or_create_by(first_name: row['first_name'], last_name: row['last_name'])
        player.bets.create!(
          status: row['stat'],
          start_time: DateTime.parse(row['start_time']),
          line: row['line'],
          pick_over: row['pick_over'] == 'true'
        )
      end
    end
  end
end
