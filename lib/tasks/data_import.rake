namespace :data_import do
  desc 'Import player and bet data'
  task import_bets: :environment do
    begin
      puts "Please wait while loading data.."
      importer = BetImporter.new(file: "public/data.csv")
      importer.import!
      puts "Succesfully imported data!"
    rescue => e
      puts "Error while importing!"
      puts e.message
    end
  end
end
