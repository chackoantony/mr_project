class CreateBets < ActiveRecord::Migration[5.1]
  def change
    create_table :bets do |t|
      t.references :player, foreign_key: true, index: true
      t.string :status
      t.datetime :start_time, index: true
      t.float :line
      t.boolean :pick_over

      t.timestamps
    end
  end
end
